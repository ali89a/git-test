@extends('layouts.admin_master')

@section('content')

<div class="box">
    <div class="box-header">
        <h3 class="box-title"><a href="{{route('role.create')}}" class="btn btn-success"><i class="fa fa-plus-square-o" aria-hidden="true"></i>&nbsp;Create Role</a></h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>SL</th>
                    <th>Name</th>
                    <th>Display Name</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1;
                ?>
                @foreach($role as $value)
                <tr>
                    <td>{{$i++}}</td>
                    <td>
                        {{$value->name}}
                    </td>
                    <td> {{$value->display_name}}</td>
                    <td> {{$value->description}}</td>
                    <td>  
                        <a href="{{route('role.edit',$value->id)}}"  class="btn btn-primary">Edit</a>
                        <button type="button"  class="btn btn-danger">Delete</button>

                    </td>
                </tr>
                @endforeach



            </tbody>
            <tfoot>
                <tr>
                    <th>SL</th>
                    <th>Name</th>
                    <th>Display Name</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.box-body -->
</div>
@endsection