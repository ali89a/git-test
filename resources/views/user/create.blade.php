@extends('layouts.admin_master')

@section('content')
<div class="box box-primary">
    <form action="{{route('role.store')}}" method="post">
        @csrf

        <div class="box-header with-border">
            <h3 class="box-title"><a href="{{route('role.index')}}" class="btn btn-success"><i class="fa fa-eye"></i>&nbsp;View Role</a></h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">

                    <div class="form-group">
                        <label for="name">Role Name</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Role Name">
                    </div>
                    <div class="form-group">
                        <label for="name">Display Name</label>
                        <input type="text" class="form-control" name="display_name" id="display_name" placeholder="Display Name">
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea class="form-control" name="description" id="description" rows="3" placeholder="Enter Description..."></textarea>
                    </div>
                    

                </div>

            </div>
            <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" value="Submit" class="btn btn-success">Submit</button>
        </div>
    </form>
</div>



@endsection