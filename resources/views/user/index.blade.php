@extends('layouts.admin_master')

@section('content')

<div class="box">
    <div class="box-header">
        <h3 class="box-title"><a href="{{route('user.create')}}" class="btn btn-success"><i class="fa fa-plus-square-o" aria-hidden="true"></i>&nbsp;Create User</a></h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>SL</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1;
                ?>
                @foreach($user as $value)
                <tr>
                    <td>{{$i++}}</td>
                    <td>{{$value->name}}</td>
                    <td> {{$value->email}}</td>
                    <td>  
                        <button type="button"  class="btn btn-primary">Edit</button>
                        <button type="button"  class="btn btn-danger">Delete</button>

                    </td>
                </tr>
                @endforeach



            </tbody>
            <tfoot>
                <tr>
                    <th>SL</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.box-body -->
</div>
@endsection